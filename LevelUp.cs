﻿using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class LevelUp : MonoBehaviour {
    MainScriptOfHeroes MainScript;
    ParticleSystem EffectLevelUp;

    HeroesStrength Health;
    Powers Power;
    IntelligenceDrone Drone;
    public TextMesh TextLevel;

    public Transform ObjectHealth;
    public Transform PowerStrength;
    public Transform PowerCapacity;
    public Transform ObjectDrone;
    public Transform ObjectAttack;
    public Transform ObjectPointHero;

    public List<SpriteRenderer> HealthLevel;
    public List<SpriteRenderer> PowerCapacityLevel;
    public List<SpriteRenderer> PowerStrengthLevel;
    public List<SpriteRenderer> DroneLevel;
    public List<SpriteRenderer> AttackLevel;
    public List<SpriteRenderer> PointsHero;

    public int HeroLevel;
    public int NumberOfHeroPoints;
    public float Experience;

    Tonemapping DarkEffect;
    float ExperienceWidth;

    public AssignedControl Control;

    string[] SplitValues;

    public ControlOfSavedGame SaveScript;

    void Start()
    {
        Health = GetComponentInChildren<HeroesStrength>();
        Power = GetComponentInChildren<Powers>();
        Drone = GetComponentInChildren<IntelligenceDrone>();
        // Wartosci---Punkty : Zycie : Moc : Atak : Zbiornik : Doswiadczenie.
        MainScript = GetComponentInParent<MainScriptOfHeroes>();
        EffectLevelUp = GetComponentInChildren<ParticleSystem>();
        DarkEffect = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Tonemapping>();
        StatisticsSprite(HealthLevel, ObjectHealth);
        StatisticsSprite(PowerStrengthLevel, PowerStrength);
        StatisticsSprite(PowerCapacityLevel, PowerCapacity);
        StatisticsSprite(DroneLevel, ObjectDrone);
        StatisticsSprite(AttackLevel, ObjectAttack);
        StatisticsSprite(PointsHero, ObjectPointHero);
        SplitValues = new string[6];
        TextLevel.text = (HeroLevel + 1).ToString();
    }

    void StatisticsSprite(List<SpriteRenderer> attribute, Transform Object)
    {
        for (int i = 0; i < Object.childCount; i++)
        {
            attribute.Add(Object.GetChild(i).GetComponent<SpriteRenderer>());
        }
    }

    public void Loading(string Values)
    {
        //Odczyt wartosci z zapisu.
        SplitValues = Values.Split(' ');
        NumberOfHeroPoints = int.Parse(SplitValues[0]);
        Health.Level = int.Parse(SplitValues[1]);
        Power.LevelCapacity = int.Parse(SplitValues[2]);
        MainScript.LevelAttack = int.Parse(SplitValues[3]);
        Drone.Level = int.Parse(SplitValues[4]);
        Experience = float.Parse(SplitValues[5]);
        HeroLevel = Health.Level + Power.LevelCapacity + Drone.Level + MainScript.LevelAttack;
        Power.LevelPowers = HeroLevel / 4;
        if (HeroLevel < 4) Power.LevelPowers = 0;
        else if (HeroLevel < 8)
        {
            Power.LevelPowers = 1;
            PowerStrengthLevel[0].enabled = true;
            TextLevel.color = Color.green;
        }
        else if (HeroLevel < 12)
        {
            for (int i = 0; i <= 1; i++)
            {
                PowerStrengthLevel[i].enabled = true;
            }
            Power.LevelPowers = 2;
            TextLevel.color = Color.yellow;
        }
        else
        {
            for (int i = 0; i <= 2; i++)
            {
                PowerStrengthLevel[i].enabled = true;
            }
            Power.LevelPowers = 3;
            TextLevel.fontSize = 50;
            TextLevel.text = "Max";
            TextLevel.color = Color.red;
        }
        Statistics(PointsHero, NumberOfHeroPoints);
        Statistics(HealthLevel, Health.Level);
        Statistics(PowerCapacityLevel, Power.LevelCapacity);
        Statistics(AttackLevel, MainScript.LevelAttack);
        Statistics(DroneLevel, Drone.Level);
        if (HeroLevel < 12) TextLevel.text = (HeroLevel + 1).ToString();
        else TextLevel.text = "Max";
    }

    void Update()
    {
        // Dodanie punktu łowcy po uzyskaniu odpowiedniej ilości.
        if ((Experience > 5) && (HeroLevel + NumberOfHeroPoints < 12))
        {
            EffectLevelUp.Play();
            NumberOfHeroPoints++;
            Statistics(PointsHero, NumberOfHeroPoints);
            Experience = 0;
            SaveValues();
        }// Obliczanie dostosowujące szerokość paska dośwadczenia.
        else ExperienceWidth = (((Experience * 1280) / 5) * Screen.width) / 1280;
    }

    //Aktywacja maszyny ulepszającej.
    public void Growth()
    {
        if ((DarkEffect.exposureAdjustment < 0.3f) && (NumberOfHeroPoints != 0))
        {
            if (((Control.Jump) || (Control.Down)) && (Health.Level < 3)) Points(1);
            else if (((Control.Charging) || (Control.Up)) && (Power.LevelCapacity < 3)) Points(2);
            else if (((Control.Use) || (Control.Right)) && (Drone.Level < 3)) Points(3);
            else if (((Control.Attack) || (Control.Left)) && (MainScript.LevelAttack < 3)) Points(4);
        }
        else DarkEffect.exposureAdjustment -= 0.01f;
    }
    void Points(int Choose)
    {
        // Ulepszanie wybranej umiejętnosci.
        NumberOfHeroPoints--;
        PointsHero[NumberOfHeroPoints].enabled = false;
        DarkEffect.enabled = false;
        DarkEffect.exposureAdjustment = 1;
        HeroLevel++;
        TextLevel.text = (HeroLevel + 1).ToString();
        DarkEffect.enabled = false;
        switch (Choose)
        {
            case 1:
                HealthLevel[Health.Level].enabled = true;
                Health.Level++;
                Health.MaxHealth = 100 + (Health.Level * 25);
                Health.MaxFatigue = 100 + (Health.Level * 25);
                break;
            case 2:
                PowerCapacityLevel[Power.LevelCapacity].enabled = true;
                Power.LevelCapacity++;
                Power.MaximumCapacity = Power.LevelCapacity * 100;
                break;
            case 3:
                DroneLevel[Drone.Level].enabled = true;
                Drone.Level++;
                Drone.LightCapacity = 35 + (35 * Drone.Level);
                Drone.CureCapacity = 35 + (35 * Drone.Level);
                break;
            case 4:
                AttackLevel[MainScript.LevelAttack].enabled = true;
                MainScript.LevelAttack++;
                break;
        }
        // Zwiekszenie poziomu mocy.
        switch (HeroLevel)
        {
            case 4:
                {
                    PowerStrengthLevel[0].enabled = true;
                    Power.LevelPowers++;
                    TextLevel.color = Color.green;
                    Statistics(PowerStrengthLevel, Power.LevelPowers);
                }
                break;
            case 8:
                {
                    Power.LevelPowers++;
                    Statistics(PowerStrengthLevel, Power.LevelPowers);
                    TextLevel.color = Color.yellow;
                }
                break;
            case 12:
                {
                    Power.LevelPowers++;
                    Statistics(PowerStrengthLevel, Power.LevelPowers);
                    TextLevel.fontSize = 50;
                    TextLevel.text = "Max";
                    TextLevel.color = Color.red;
                    this.enabled = false;
                }
                break;
        }            
        SaveValues();
    }

    public void SaveValues()
    {
        // Zapis danych.
        SaveScript.SendMessage(LayerMask.LayerToName(gameObject.layer),
        ((NumberOfHeroPoints)
        + " " + (Health.Level)
        + " " + (Power.LevelCapacity)
        + " " + (MainScript.LevelAttack)
        + " " + (Drone.Level)
        + " " + (Experience)));
    }

    void IncreaseTheBar(float EnemiesExperience)
    {
        Experience += EnemiesExperience;
    }

    private void OnApplicationQuit()
    {
        SaveValues();
    }

    void Statistics(List<SpriteRenderer> Sprite, int Level)
    {
        // Pętla pokazująca atrybuty bohaterów na maszynie ulepszającej.
        for (int i = 0; i < Level; i++)
        {
            Sprite[i].enabled = true;
        }
    }
}
